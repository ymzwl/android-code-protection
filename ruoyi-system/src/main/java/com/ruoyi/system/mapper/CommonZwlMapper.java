package com.ruoyi.system.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CommonZwlMapper {
    public List<HashMap<Object, Object>> list(Map<String, Object> map);
    /**
     * @return 返回被操作记录条数
     */
    public int insert(Map<String, Object> map);
    /**
     * @return 返回被操作记录条数
     */
    public int update(Map<String, Object> map);
    /**
     * @return 返回被操作记录条数
     */
    public int delete(Map<String, Object> map);
}
