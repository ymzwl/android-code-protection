package com.ruoyi.system.service;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.core.domain.entity.SysUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户 业务层
 *
 * @author ruoyi
 */
public interface ICommonZwlService
{
    public JSONArray list(String sql);
}
