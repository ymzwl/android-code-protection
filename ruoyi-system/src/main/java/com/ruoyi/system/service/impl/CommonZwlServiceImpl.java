package com.ruoyi.system.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.utils.UtilZwl;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.ICommonZwlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户 业务层处理
 *
 * @author ruoyi
 */
@Service
public class CommonZwlServiceImpl implements ICommonZwlService
{

    @Autowired
    private CommonZwlMapper commonZwlMapper;

    @Override
    public JSONArray list(String sql) {
        Map map = new HashMap();
        map.put("sql", sql);
        return UtilZwl.list_to_JSONArray(commonZwlMapper.list(map));
    }

}
