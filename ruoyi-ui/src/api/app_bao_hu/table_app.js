import request from '@/utils/request'

// 查询app上传列表
export function listTable_app(query) {
  return request({
    url: '/app_bao_hu/table_app/list',
    method: 'get',
    params: query
  })
}

// 查询app上传详细
export function getTable_app(appId) {
  return request({
    url: '/app_bao_hu/table_app/' + appId,
    method: 'get'
  })
}

// 新增app上传
export function addTable_app(data) {
  return request({
    url: '/app_bao_hu/table_app',
    method: 'post',
    data: data
  })
}

// 修改app上传
export function updateTable_app(data) {
  return request({
    url: '/app_bao_hu/table_app',
    method: 'put',
    data: data
  })
}

// 删除app上传
export function delTable_app(appId) {
  return request({
    url: '/app_bao_hu/table_app/' + appId,
    method: 'delete'
  })
}
