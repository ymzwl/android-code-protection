import request from '@/utils/request'

// 查询apk安装信息列表
export function listTable_an_zhuang(query) {
  return request({
    url: '/app_bao_hu/table_an_zhuang/list',
    method: 'get',
    params: query
  })
}

// 获取篡改数据
export function get_cuan_gai_shu_ju(query) {
  return request({
    url: '/app_bao_hu/table_an_zhuang/get_cuan_gai_shu_ju',
    method: 'get',
    params: query
  })
}

// 查询apk安装信息详细
export function getTable_an_zhuang(anZhuangId) {
  return request({
    url: '/app_bao_hu/table_an_zhuang/' + anZhuangId,
    method: 'get'
  })
}

// 新增apk安装信息
export function addTable_an_zhuang(data) {
  return request({
    url: '/app_bao_hu/table_an_zhuang',
    method: 'post',
    data: data
  })
}

// 修改apk安装信息
export function updateTable_an_zhuang(data) {
  return request({
    url: '/app_bao_hu/table_an_zhuang',
    method: 'put',
    data: data
  })
}

// 删除apk安装信息
export function delTable_an_zhuang(anZhuangId) {
  return request({
    url: '/app_bao_hu/table_an_zhuang/' + anZhuangId,
    method: 'delete'
  })
}
