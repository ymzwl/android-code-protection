package com.ruoyi.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class UtilZwl {
    public static JSONArray list_to_JSONArray(List<HashMap<Object, Object>> list) {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < list.size(); i++) {
            HashMap map = (HashMap) list.get(i);
            Iterator iter = map.entrySet().iterator();
            JSONObject jsonObject = new JSONObject();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                Object key = entry.getKey();
                Object value = entry.getValue();
                if(value==null){value="";}//解决查询的值为空时候，没有字段名问题，详见spring-mybatis-configuration.xml
                jsonObject.put((String) key, value);
            }
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }

}
