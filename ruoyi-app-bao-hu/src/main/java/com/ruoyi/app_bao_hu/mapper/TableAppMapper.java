package com.ruoyi.app_bao_hu.mapper;

import java.util.List;
import com.ruoyi.app_bao_hu.domain.TableApp;

/**
 * app上传Mapper接口
 *
 * @author ruoyi
 * @date 2022-05-19
 */
public interface TableAppMapper
{
    /**
     * 查询app上传
     *
     * @param appId app上传主键
     * @return app上传
     */
    public TableApp selectTableAppByAppId(Long appId);

    /**
     * 查询app上传列表
     *
     * @param tableApp app上传
     * @return app上传集合
     */
    public List<TableApp> selectTableAppList(TableApp tableApp);

    /**
     * 新增app上传
     *
     * @param tableApp app上传
     * @return 结果
     */
    public int insertTableApp(TableApp tableApp);

    /**
     * 修改app上传
     *
     * @param tableApp app上传
     * @return 结果
     */
    public int updateTableApp(TableApp tableApp);

    /**
     * 删除app上传
     *
     * @param appId app上传主键
     * @return 结果
     */
    public int deleteTableAppByAppId(Long appId);

    /**
     * 批量删除app上传
     *
     * @param appIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTableAppByAppIds(Long[] appIds);
}
