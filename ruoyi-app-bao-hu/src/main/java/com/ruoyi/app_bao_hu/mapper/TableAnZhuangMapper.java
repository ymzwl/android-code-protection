package com.ruoyi.app_bao_hu.mapper;

import java.util.List;
import com.ruoyi.app_bao_hu.domain.TableAnZhuang;

/**
 * apk安装信息Mapper接口
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
public interface TableAnZhuangMapper 
{
    /**
     * 查询apk安装信息
     * 
     * @param anZhuangId apk安装信息主键
     * @return apk安装信息
     */
    public TableAnZhuang selectTableAnZhuangByAnZhuangId(Long anZhuangId);

    /**
     * 查询apk安装信息列表
     * 
     * @param tableAnZhuang apk安装信息
     * @return apk安装信息集合
     */
    public List<TableAnZhuang> selectTableAnZhuangList(TableAnZhuang tableAnZhuang);

    /**
     * 新增apk安装信息
     * 
     * @param tableAnZhuang apk安装信息
     * @return 结果
     */
    public int insertTableAnZhuang(TableAnZhuang tableAnZhuang);

    /**
     * 修改apk安装信息
     * 
     * @param tableAnZhuang apk安装信息
     * @return 结果
     */
    public int updateTableAnZhuang(TableAnZhuang tableAnZhuang);

    /**
     * 删除apk安装信息
     * 
     * @param anZhuangId apk安装信息主键
     * @return 结果
     */
    public int deleteTableAnZhuangByAnZhuangId(Long anZhuangId);

    /**
     * 批量删除apk安装信息
     * 
     * @param anZhuangIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTableAnZhuangByAnZhuangIds(Long[] anZhuangIds);
}
