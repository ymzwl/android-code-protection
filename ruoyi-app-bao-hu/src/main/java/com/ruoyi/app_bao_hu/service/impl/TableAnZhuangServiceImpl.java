package com.ruoyi.app_bao_hu.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app_bao_hu.mapper.TableAnZhuangMapper;
import com.ruoyi.app_bao_hu.domain.TableAnZhuang;
import com.ruoyi.app_bao_hu.service.ITableAnZhuangService;

/**
 * apk安装信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-05-19
 */
@Service
public class TableAnZhuangServiceImpl implements ITableAnZhuangService 
{
    @Autowired
    private TableAnZhuangMapper tableAnZhuangMapper;

    /**
     * 查询apk安装信息
     * 
     * @param anZhuangId apk安装信息主键
     * @return apk安装信息
     */
    @Override
    public TableAnZhuang selectTableAnZhuangByAnZhuangId(Long anZhuangId)
    {
        return tableAnZhuangMapper.selectTableAnZhuangByAnZhuangId(anZhuangId);
    }

    /**
     * 查询apk安装信息列表
     * 
     * @param tableAnZhuang apk安装信息
     * @return apk安装信息
     */
    @Override
    public List<TableAnZhuang> selectTableAnZhuangList(TableAnZhuang tableAnZhuang)
    {
        return tableAnZhuangMapper.selectTableAnZhuangList(tableAnZhuang);
    }

    /**
     * 新增apk安装信息
     * 
     * @param tableAnZhuang apk安装信息
     * @return 结果
     */
    @Override
    public int insertTableAnZhuang(TableAnZhuang tableAnZhuang)
    {
        tableAnZhuang.setCreateTime(DateUtils.getNowDate());
        return tableAnZhuangMapper.insertTableAnZhuang(tableAnZhuang);
    }

    /**
     * 修改apk安装信息
     * 
     * @param tableAnZhuang apk安装信息
     * @return 结果
     */
    @Override
    public int updateTableAnZhuang(TableAnZhuang tableAnZhuang)
    {
        tableAnZhuang.setUpdateTime(DateUtils.getNowDate());
        return tableAnZhuangMapper.updateTableAnZhuang(tableAnZhuang);
    }

    /**
     * 批量删除apk安装信息
     * 
     * @param anZhuangIds 需要删除的apk安装信息主键
     * @return 结果
     */
    @Override
    public int deleteTableAnZhuangByAnZhuangIds(Long[] anZhuangIds)
    {
        return tableAnZhuangMapper.deleteTableAnZhuangByAnZhuangIds(anZhuangIds);
    }

    /**
     * 删除apk安装信息信息
     * 
     * @param anZhuangId apk安装信息主键
     * @return 结果
     */
    @Override
    public int deleteTableAnZhuangByAnZhuangId(Long anZhuangId)
    {
        return tableAnZhuangMapper.deleteTableAnZhuangByAnZhuangId(anZhuangId);
    }
}
