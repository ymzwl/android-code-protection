package com.ruoyi.app_bao_hu.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.app_bao_hu.mapper.TableAppMapper;
import com.ruoyi.app_bao_hu.domain.TableApp;
import com.ruoyi.app_bao_hu.service.ITableAppService;

/**
 * app上传Service业务层处理
 *
 * @author ruoyi
 * @date 2022-05-19
 */
@Service
public class TableAppServiceImpl implements ITableAppService
{
    @Autowired
    private TableAppMapper tableAppMapper;

    /**
     * 查询app上传
     *
     * @param appId app上传主键
     * @return app上传
     */
    @Override
    public TableApp selectTableAppByAppId(Long appId)
    {
        return tableAppMapper.selectTableAppByAppId(appId);
    }

    /**
     * 查询app上传列表
     *
     * @param tableApp app上传
     * @return app上传
     */
    @Override
    public List<TableApp> selectTableAppList(TableApp tableApp)
    {
        return tableAppMapper.selectTableAppList(tableApp);
    }

    /**
     * 新增app上传
     *
     * @param tableApp app上传
     * @return 结果
     */
    @Override
    public int insertTableApp(TableApp tableApp)
    {
        tableApp.setCreateTime(DateUtils.getNowDate());
        return tableAppMapper.insertTableApp(tableApp);
    }

    /**
     * 修改app上传
     *
     * @param tableApp app上传
     * @return 结果
     */
    @Override
    public int updateTableApp(TableApp tableApp)
    {
        tableApp.setUpdateTime(DateUtils.getNowDate());
        return tableAppMapper.updateTableApp(tableApp);
    }

    /**
     * 批量删除app上传
     *
     * @param appIds 需要删除的app上传主键
     * @return 结果
     */
    @Override
    public int deleteTableAppByAppIds(Long[] appIds)
    {
        return tableAppMapper.deleteTableAppByAppIds(appIds);
    }

    /**
     * 删除app上传信息
     *
     * @param appId app上传主键
     * @return 结果
     */
    @Override
    public int deleteTableAppByAppId(Long appId)
    {
        return tableAppMapper.deleteTableAppByAppId(appId);
    }
}
