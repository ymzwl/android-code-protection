package com.ruoyi.app_bao_hu.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * apk安装信息对象 table_an_zhuang
 *
 * @author ruoyi
 * @date 2022-05-19
 */
public class TableAnZhuang extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long anZhuangId;

    /** 安装apk设备的id */
    @Excel(name = "安装apk设备的id")
    private String sheBeiId;

    /** 安装apk的包名，和table_app中的bao_ming，多对一 */
    @Excel(name = "安装apk的包名，和table_app中的bao_ming，多对一")
    private String baoMing;

    /** 安装apk的签名 */
    @Excel(name = "安装apk的签名")
    private String qianMing;

    /** 0apk没有被篡改，1apk被篡改 */
    @Excel(name = "0apk没有被篡改，1apk被篡改")
    private Integer shiFouChuanGai;

    public void setAnZhuangId(Long anZhuangId)
    {
        this.anZhuangId = anZhuangId;
    }

    public Long getAnZhuangId()
    {
        return anZhuangId;
    }
    public void setSheBeiId(String sheBeiId)
    {
        this.sheBeiId = sheBeiId;
    }

    public String getSheBeiId()
    {
        return sheBeiId;
    }
    public void setBaoMing(String baoMing)
    {
        this.baoMing = baoMing;
    }

    public String getBaoMing()
    {
        return baoMing;
    }
    public void setQianMing(String qianMing)
    {
        this.qianMing = qianMing;
    }

    public String getQianMing()
    {
        return qianMing;
    }
    public void setShiFouChuanGai(Integer shiFouChuanGai)
    {
        this.shiFouChuanGai = shiFouChuanGai;
    }

    public Integer getShiFouChuanGai()
    {
        return shiFouChuanGai;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("anZhuangId", getAnZhuangId())
                .append("sheBeiId", getSheBeiId())
                .append("baoMing", getBaoMing())
                .append("qianMing", getQianMing())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("shiFouChuanGai", getShiFouChuanGai())
                .toString();
    }
}
