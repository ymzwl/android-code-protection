package com.ruoyi.app_bao_hu.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * app上传对象 table_app
 *
 * @author ruoyi
 * @date 2022-05-19
 */
public class TableApp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**  */
    private Long appId;

    /** 包名 */
    @Excel(name = "包名")
    private String baoMing;

    /** apk签名 */
    @Excel(name = "apk签名")
    private String qianMing;

    public void setAppId(Long appId)
    {
        this.appId = appId;
    }

    public Long getAppId()
    {
        return appId;
    }
    public void setBaoMing(String baoMing)
    {
        this.baoMing = baoMing;
    }

    public String getBaoMing()
    {
        return baoMing;
    }
    public void setQianMing(String qianMing)
    {
        this.qianMing = qianMing;
    }

    public String getQianMing()
    {
        return qianMing;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("appId", getAppId())
            .append("baoMing", getBaoMing())
            .append("qianMing", getQianMing())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
