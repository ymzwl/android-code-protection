package com.ruoyi.web.controller.app_bao_hu;

import com.ruoyi.app_bao_hu.domain.TableAnZhuang;
import com.ruoyi.app_bao_hu.domain.TableApp;
import com.ruoyi.app_bao_hu.service.ITableAnZhuangService;
import com.ruoyi.app_bao_hu.service.ITableAppService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * apk安装信息Controller
 *
 * @author ruoyi
 * @date 2022-05-19
 */
@RestController
@RequestMapping("/app_bao_hu/table_an_zhuang_mei_quan_xian")
public class TableAnZhuangMeiQuanXianController extends BaseController
{
    @Autowired
    private ITableAnZhuangService tableAnZhuangService;

    @Autowired
    private ITableAppService tableAppService;

    /**
     * 查询apk安装信息列表
     */
    //@PreAuthorize("@ss.hasPermi('app_bao_hu:table_an_zhuang:list')")
    @GetMapping("/list")
    public TableDataInfo list(TableAnZhuang tableAnZhuang)
    {
        startPage();
        List<TableAnZhuang> list = tableAnZhuangService.selectTableAnZhuangList(tableAnZhuang);
        return getDataTable(list);
    }

    /**
     * 导出apk安装信息列表
     */
    //@PreAuthorize("@ss.hasPermi('app_bao_hu:table_an_zhuang:export')")
    @Log(title = "apk安装信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TableAnZhuang tableAnZhuang)
    {
        List<TableAnZhuang> list = tableAnZhuangService.selectTableAnZhuangList(tableAnZhuang);
        ExcelUtil<TableAnZhuang> util = new ExcelUtil<TableAnZhuang>(TableAnZhuang.class);
        util.exportExcel(response, list, "apk安装信息数据");
    }

    /**
     * 获取apk安装信息详细信息
     */
    //@PreAuthorize("@ss.hasPermi('app_bao_hu:table_an_zhuang:query')")
    @GetMapping(value = "/{anZhuangId}")
    public AjaxResult getInfo(@PathVariable("anZhuangId") Long anZhuangId)
    {
        return AjaxResult.success(tableAnZhuangService.selectTableAnZhuangByAnZhuangId(anZhuangId));
    }

    /**
     * 新增apk安装信息
     */
//    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_an_zhuang:add')")
    @Log(title = "apk安装信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestParam() String msg)
    {
        //msg:设备id_签名_包名
        String[] msgs=msg.split("_");
        TableAnZhuang tableAnZhuang=new TableAnZhuang();
        tableAnZhuang.setSheBeiId(msgs[0]);
        tableAnZhuang.setQianMing(msgs[1]);
        tableAnZhuang.setBaoMing(msgs[2]);

        TableApp tableApp=new TableApp();
        tableApp.setBaoMing(msgs[2]);
        tableApp.setQianMing(msgs[1]);
        List<TableApp> l1=tableAppService.selectTableAppList(tableApp);
        if(l1.size()>0){
            tableAnZhuang.setShiFouChuanGai(0);
        }else{
            tableAnZhuang.setShiFouChuanGai(1);
        }



        return toAjax(tableAnZhuangService.insertTableAnZhuang(tableAnZhuang));
    }

    /**
     * 修改apk安装信息
     */
    //@PreAuthorize("@ss.hasPermi('app_bao_hu:table_an_zhuang:edit')")
    @Log(title = "apk安装信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TableAnZhuang tableAnZhuang)
    {
        return toAjax(tableAnZhuangService.updateTableAnZhuang(tableAnZhuang));
    }

    /**
     * 删除apk安装信息
     */
    //@PreAuthorize("@ss.hasPermi('app_bao_hu:table_an_zhuang:remove')")
    @Log(title = "apk安装信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{anZhuangIds}")
    public AjaxResult remove(@PathVariable Long[] anZhuangIds)
    {
        return toAjax(tableAnZhuangService.deleteTableAnZhuangByAnZhuangIds(anZhuangIds));
    }
}
