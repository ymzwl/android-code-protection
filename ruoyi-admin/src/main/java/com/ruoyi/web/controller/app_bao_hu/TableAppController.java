package com.ruoyi.web.controller.app_bao_hu;

import com.ruoyi.app_bao_hu.domain.TableApp;
import com.ruoyi.app_bao_hu.service.ITableAppService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.config.ServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * app上传Controller
 *
 * @author ruoyi
 * @date 2022-05-19
 */
@RestController
@RequestMapping("/app_bao_hu/table_app")
public class TableAppController extends BaseController
{
    @Autowired
    private ITableAppService tableAppService;

    @Autowired
    private ServerConfig serverConfig;

    /**
     * 查询app上传列表
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:list')")
    @GetMapping("/list")
    public TableDataInfo list(TableApp tableApp)
    {
        startPage();
        tableApp.setCreateBy(getUsername());
        List<TableApp> list = tableAppService.selectTableAppList(tableApp);
        return getDataTable(list);
    }

    /**
     * 导出app上传列表
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:export')")
    @Log(title = "app上传", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TableApp tableApp)
    {
        List<TableApp> list = tableAppService.selectTableAppList(tableApp);
        ExcelUtil<TableApp> util = new ExcelUtil<TableApp>(TableApp.class);
        util.exportExcel(response, list, "app上传数据");
    }

    /**
     * 获取app上传详细信息
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:query')")
    @GetMapping(value = "/{appId}")
    public AjaxResult getInfo(@PathVariable("appId") Long appId)
    {
        return AjaxResult.success(tableAppService.selectTableAppByAppId(appId));
    }

    /**
     * 新增app上传
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:add')")
    @Log(title = "app上传", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TableApp tableApp)
    {
        tableApp.setCreateBy(getUsername());
        return toAjax(tableAppService.insertTableApp(tableApp));
    }

    /**
     * 修改app上传
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:edit')")
    @Log(title = "app上传", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TableApp tableApp)
    {
        return toAjax(tableAppService.updateTableApp(tableApp));
    }

    /**
     * 删除app上传
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:remove')")
    @Log(title = "app上传", businessType = BusinessType.DELETE)
	@DeleteMapping("/{appIds}")
    public AjaxResult remove(@PathVariable Long[] appIds)
    {
        return toAjax(tableAppService.deleteTableAppByAppIds(appIds));
    }

    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    //@PreAuthorize("@ss.hasPermi('system:user:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {

        // 上传文件路径
        String filePath = RuoYiConfig.getUploadPath();
        // 上传并返回新文件名称
        String fileName = FileUploadUtils.upload(filePath, file);
        String url = serverConfig.getUrl() + fileName;

        String absolutePath=filePath+fileName.replaceAll("/profile/upload","");

        StringBuffer stringBuffer=new StringBuffer();

        Process p = Runtime.getRuntime().exec("java -jar D:\\Users\\signaturetool-md5.jar "+absolutePath);
        InputStream is = p.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        while((line = reader.readLine())!= null){
            //System.out.println(line);
            stringBuffer.append(line);
        }
        p.waitFor();
        is.close();
        reader.close();
        p.destroy();



        String md5=stringBuffer.substring(stringBuffer.length()-32,stringBuffer.length());

        System.out.println(md5);

        AjaxResult ajax = AjaxResult.success();

        ajax.put("md5",md5);

//        ajax.put("url", url);
//        ajax.put("fileName", fileName);
//        ajax.put("newFileName", FileUtils.getName(fileName));
//        ajax.put("originalFilename", file.getOriginalFilename());

//        ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
//        List<SysUser> userList = util.importExcel(file.getInputStream());
//        String operName = getUsername();
//        String message = userService.importUser(userList, updateSupport, operName);

//        StringBuilder successMsg = new StringBuilder();
//        successMsg.insert(0, "签名");
//        return AjaxResult.success(successMsg);
        return ajax;
    }


}
