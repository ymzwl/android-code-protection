package com.ruoyi.web.controller.app_bao_hu;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.system.service.ICommonZwlService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.app_bao_hu.domain.TableAnZhuang;
import com.ruoyi.app_bao_hu.service.ITableAnZhuangService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * apk安装信息Controller
 *
 * @author ruoyi
 * @date 2022-05-19
 */
@RestController
@RequestMapping("/app_bao_hu/table_an_zhuang")
public class TableAnZhuangController extends BaseController
{
    @Autowired
    private ITableAnZhuangService tableAnZhuangService;

    @Autowired
    ICommonZwlService commonZwlService;

    /**
     * 查询apk安装信息列表
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:list')")
    @GetMapping("/list")
    public TableDataInfo list(TableAnZhuang tableAnZhuang)
    {
        startPage();
        List<TableAnZhuang> list = tableAnZhuangService.selectTableAnZhuangList(tableAnZhuang);
        return getDataTable(list);
    }

    /**
     * 获取篡改数据
     * let data=[{name:"被篡改",value:2},{name:"正常",value:5}];
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:list')")
    @GetMapping("/get_cuan_gai_shu_ju")
    public AjaxResult get_cuan_gai_shu_ju(TableAnZhuang tableAnZhuang)
    {
        JSONArray ja=commonZwlService.list("select shi_fou_chuan_gai,COUNT(*) as count from table_an_zhuang where bao_ming=\""+tableAnZhuang.getBaoMing()+"\" GROUP BY shi_fou_chuan_gai");
        return AjaxResult.success(ja.toString());
    }

    /**
     * 导出apk安装信息列表
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:list')")
    @Log(title = "apk安装信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TableAnZhuang tableAnZhuang)
    {
        List<TableAnZhuang> list = tableAnZhuangService.selectTableAnZhuangList(tableAnZhuang);
        ExcelUtil<TableAnZhuang> util = new ExcelUtil<TableAnZhuang>(TableAnZhuang.class);
        util.exportExcel(response, list, "apk安装信息数据");
    }

    /**
     * 获取apk安装信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:list')")
    @GetMapping(value = "/{anZhuangId}")
    public AjaxResult getInfo(@PathVariable("anZhuangId") Long anZhuangId)
    {
        return AjaxResult.success(tableAnZhuangService.selectTableAnZhuangByAnZhuangId(anZhuangId));
    }

    /**
     * 新增apk安装信息
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:list')")
    @Log(title = "apk安装信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TableAnZhuang tableAnZhuang)
    {
        return toAjax(tableAnZhuangService.insertTableAnZhuang(tableAnZhuang));
    }

    /**
     * 修改apk安装信息
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:list')")
    @Log(title = "apk安装信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TableAnZhuang tableAnZhuang)
    {
        return toAjax(tableAnZhuangService.updateTableAnZhuang(tableAnZhuang));
    }

    /**
     * 删除apk安装信息
     */
    @PreAuthorize("@ss.hasPermi('app_bao_hu:table_app:list')")
    @Log(title = "apk安装信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{anZhuangIds}")
    public AjaxResult remove(@PathVariable Long[] anZhuangIds)
    {
        return toAjax(tableAnZhuangService.deleteTableAnZhuangByAnZhuangIds(anZhuangIds));
    }
}
